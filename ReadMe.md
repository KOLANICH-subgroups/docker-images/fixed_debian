Scripts for building an image containing Debian testing and needed libs and tools
==============================================================

Scripts in this repo are [Unlicensed ![](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/).

Third-party components have own licenses.

**DISCLAIMER: BADGES BELOW DO NOT REFLECT THE STATE OF THE DEPENDENCIES IN THE CONTAINER**

This builds a Docker image contains the following components:
  * [`apt-transport-https`](https://tracker.debian.org/pkg/apt) [![Debian Testing package](https://repology.org/badge/version-for-repo/debian_testing/apt.svg) ![latest packaged version(s)](https://repology.org/badge/latest-versions/apt.svg)](https://repology.org/metapackage/apt/versions) [from Debian packages](https://packages.debian.org/search?keywords=apt-transport-https) [![GNU General Public License v2](https://img.shields.io/badge/License-GPL--2.0-fe7d37.svg)](./licenses/GPL-2.0.md);
  * [`gnupg1-curl`](https://tracker.debian.org/pkg/gnupg1) [![Debian Testing package](https://repology.org/badge/version-for-repo/debian_testing/gnupg1.svg) ![latest packaged version(s)](https://repology.org/badge/latest-versions/gnupg1.svg)](https://repology.org/metapackage/gnupg1/versions) [from Debian packages](https://packages.debian.org/search?keywords=gnupg%20-curl) [![GNU General Public License v3](https://www.gnu.org/graphics/gplv3-88x31.png)](./licenses/GPL-3.0.md);
  * some settings
