import os
from pathlib import Path
import csv
import subprocess

dv = Path(os.environ.get("LSB_ETC_DEBIAN_VERSION", "/etc/debian_version"))
dc = Path("/usr/share/distro-info/debian.csv")


def getVersion(codeName: str) -> str:
	with dc.open("rt", encoding="utf-8") as f:
		for r in csv.DictReader(f):
			if r["series"].lower() == codeName:
				res = r["version"].strip().split(".")
				if len(res) == 1:
					res = res[0] + ".0"
				else:
					if len(res) > 3:
						res = res[:3]
					res = ".".join(res)
				return res

def main():
	release = subprocess.run(["lsb_release", "-sc"], capture_output=True, encoding="utf-8").stdout.strip().lower()  # GPL fans are no better than proprietary licenses fans

	print("Release using lsb_release: ", release)
	if release != "sid":
		dv.write_text(str(getVersion(release)))

if __name__ == "__main__":
	main()
