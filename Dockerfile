# syntax=docker/dockerfile:experimental
#FROM python:rc # problems with cython
FROM debian:unstable-slim
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./fix_debian_version.py ./
RUN \
  set -ex;\
  echo "APT::Get::Install-Recommends \" false\";" > /etc/apt/apt.conf ;\
  echo "APT::Get::Install-Suggests \" false\";" > /etc/apt/apt.conf ;\
  apt-get update ;\
  apt-get install -y ca-certificates apt-transport-https;\
  sed -i 's%http://deb.debian.org%https://mirrors.edge.kernel.org%g' /etc/apt/sources.list.d/debian.sources;\
  sed -i 's%http://%https://%g' /etc/apt/sources.list.d/debian.sources;\
  apt-get update ;\
  apt-get install -y --reinstall apt-transport-https ca-certificates;\
  . /etc/os-release;\
  apt-get install -y lsb-release distro-info-data python3-minimal;\
  python3 fix_debian_version.py;\
  cat /etc/debian_version;\
  export DEBIAN_RELEASE=$(lsb_release -sc);\
  if [ "$DEBIAN_RELEASE" != "sid" ]; then\
    echo "deb https://mirrors.kernel.org/debian $DEBIAN_RELEASE-backports main contrib" >> /etc/apt/sources.list.d/debian.sources;\
  fi;\
  apt remove -y python3-minimal;\
  apt-get update ;\
  apt-get install -y git aria2 gnupg1-curl curl gpg;\
  git config --system user.email CI@docker;\
  git config --system user.name CI;\
  git config --system init.defaultBranch master;\
  mkdir -p ~/.gnupg;\
  touch ~/.gnupg/gpg.conf;\
  chmod -R 600 ~/.gnupg;\
  echo use-keyboxd >> ~/.gnupg/common.conf;\
  echo keyid-format none >> ~/.gnupg/gpg.conf;\
  echo with-fingerprint >> ~/.gnupg/gpg.conf;\
  echo personal-cipher-preferences AES256 >> ~/.gnupg/gpg.conf;\
  echo cert-digest-algo SHA512 >> ~/.gnupg/gpg.conf;\
  echo default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed >> ~/.gnupg/gpg.conf;\
  echo personal-digest-preferences AES256 >> ~/.gnupg/gpg.conf;\
  rm -rf /usr/share/git-core/templates/hooks;\
  rm -rf /usr/share/gnupg/help.*.txt;\
  rm -rf /usr/share/gnupg/help.*.txt;\
  apt-get autoremove --purge -y ;\
  apt-get clean && rm -rf /var/lib/apt/lists/*;\
  rm -rf /tmp/*
